<?php
/**
 * @file ajax_timeline.module
 *
 * Creates an ajax loadable timeline of a content type
 */
 
/**
 * Implements hook_menu().
 */ 
function ajax_timeline_menu() {
  $items = array();
  $items['admin/config/media/ajax_timeline'] = array(
    'title' => 'Ajax Timeline Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ajax_timeline_admin_form'),
    'access arguments' => array('ajax timeline'),
    'file' => 'ajax_timeline.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['ajax/ajax_timeline/%'] = array(
    'page callback' => 'ajax_timeline_load_events',
    'page arguments' => array(2, TRUE),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_permission()
 */
function ajax_timeline_permission() {
  return array(
    'ajax timeline' => array(
      'title' => t('Administer Ajax Timeline'),
      'description' => t('Change settings for the Ajax Timeline Module'),
    ), 
  );
}

/**
 * Implements hook_block_info().
 */
function ajax_timeline_block_info() {
  $blocks = array();
  $blocks['ajax_timeline'] = array(
    'info' => t('Ajax Timeline'),
  );
  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function ajax_timeline_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'ajax_timeline':
      $block['content'] = array(
        '#markup' => ajax_timeline_block_content(),
        '#attached' => array(
          'css' => array(
            drupal_get_path('module', 'ajax_timeline') . '/css/ajax_timeline.css',
          ),
          'js' => array(
            drupal_get_path('module', 'ajax_timeline') . '/js/ajax_timeline.js',
            array(
              'data' => array(
                'ajaxTimeline' => array('infiniteScroll' => variable_get('ajax_timeline_infinite_scroll', FALSE)),
              ), 
              'type' => 'setting',
            ),
          ),
        ),
      );
      break;
  }
  return $block;
}

/**
 * Implements image_default_styles().
 */
function ajax_timeline_image_default_styles() {
  $styles = array();

  $styles['ajax_timeline'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 375,
          'height' => 150,
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Output the event timeline
 */
function ajax_timeline_block_content() {
  // Add libraries 
  drupal_add_library('system', 'drupal.ajax'); 
  drupal_add_library('system', 'jquery.form');
  
  $output = '<div id="ajax-timeline-wrapper">';
  $output .= ajax_timeline_load_events(date('Y'));
  $output .= '</div>';
  
  return $output;
}

/**
 * Load up all events for a given year
 */
function ajax_timeline_load_events($year, $ajax = FALSE) {
  global $language;
  $is_field = FALSE;
  $timeline_type = check_plain(variable_get('ajax_timeline_type', 'page'));
  $timeline_field = check_plain(variable_get('ajax_timeline_field', 'created'));
  $timeline_image = check_plain(variable_get('ajax_timeline_image', ''));
  $db_type = Database::getConnection()->databaseType();
  
  // Field structure vs created/changed.
  if (strpos($timeline_field, 'field_') !== FALSE) {
    $is_field = TRUE; 
  }

  if ($db_type == 'pgsql') {
    if ($is_field) {
      $query = "SELECT DISTINCT date_part('year', " . $timeline_field . "_value) AS year FROM {field_data_" . $timeline_field . "} ";
      $query .= "WHERE language IN (:language) ";
      $query .= "ORDER BY year DESC";
    }
    else {
      $query = "SELECT DISTINCT date_part('year', to_timestamp(" . $timeline_field . ")) AS year FROM {node} ";
      $query .= "WHERE language IN (:language) ";
      $query .= "ORDER BY year DESC";      
    }
    $event_years = db_query($query, array(':language' => array(LANGUAGE_NONE, $language->language)));
  }
  else {
    if ($is_field) {
      $query = db_select('field_data_' . $timeline_field, 'f')->condition('bundle', $timeline_type, '=')->orderBy('year', 'DESC');
      $or = db_or();
      $or->condition('language', $language->language, '=');
      $or->condition('language', LANGUAGE_NONE, '=');
      $query->condition($or);
      $query->AddExpression('DISTINCT YEAR(f.' . $timeline_field . '_value)', 'year');
    }
    else {
      $query = db_select('node', 'n')->condition('type', $timeline_type, '=')->orderBy('year', 'DESC');
      $or = db_or();
      $or->condition('language', $language->language, '=');
      $or->condition('language', LANGUAGE_NONE, '=');
      $query->condition($or);
      $query->AddExpression('DISTINCT YEAR(FROM_UNIXTIME(' . $timeline_field . '))', 'year');
    }    
    $event_years = $query->execute();    
  }

  $event_list = '';
  foreach ($event_years as $event_year) {
    if ($event_year->year == $year) {
      if ($db_type == 'pgsql') {
        if ($is_field) {
          $query = "SELECT nid FROM {node} ";
          $query .= "JOIN {field_data_" . $timeline_field . "} d ON d.entity_id = nid ";
          $query .= "WHERE status = :status AND type = :type AND node.language IN (:language) AND d.language IN (:language) ";
          $query .= "AND date_part('year', " . $timeline_field . "_value) = :year ";
          $query .= "ORDER BY " . $timeline_field . "_value DESC";
        }
        else {
          $query = "SELECT nid FROM {node} ";
          $query .= "WHERE status = :status AND type = :type AND language IN (:language) ";
          $query .= "AND date_part('year', to_timestamp(" . $timeline_field . ")) = :year ";
          $query .= "ORDER BY " . $timeline_field . " DESC";
        }
        $nids = db_query($query, array(':status' => 1, ':type' => $timeline_type, ':language' => array(LANGUAGE_NONE, $language->language), ':year' => $year));
      }
      else {
        if ($is_field) {
          $query = db_select('node', 'n');
          $query->join('field_data_' . $timeline_field, 'd', 'd.entity_id = n.nid');
          $query
            ->fields('n', array('nid'))
            ->condition('n.type', $timeline_type, '=')
            ->condition('n.status', 1, '=')
            ->where('YEAR(d.' . $timeline_field . '_value) = :year', array(':year' => $year), 'formula')
            ->orderBY('d.' . $timeline_field . '_value', 'DESC');
          $or = db_or();
          $or->condition('d.language', $language->language, '=');
          $or->condition('d.language', LANGUAGE_NONE, '=');
          $query->condition($or);
          $or = db_or();
          $or->condition('n.language', $language->language, '=');
          $or->condition('n.language', LANGUAGE_NONE, '=');
          $query->condition($or);
        }
        else {
          $query = db_select('node', 'n')
            ->fields('n', array('nid'))
            ->condition('type', $timeline_type, '=')
            ->condition('status', 1, '=')
            ->where('YEAR(FROM_UNIXTIME(' . $timeline_field . ')) = :year', array(':year' => $year), 'formula')
            ->orderBY($timeline_field, 'DESC');
            $or = db_or();
            $or->condition('language', $language->language, '=');
            $or->condition('language', LANGUAGE_NONE, '=');
            $query->condition($or);
        }
        // Get all nids of selected year.
        $nids = $query->execute();
      }
    
      if (!$ajax) {
        $event_list .= '<li id="year-' . $event_year->year . '" class="year current-year loaded"><a href="/ajax/ajax_timeline/' . $event_year->year . '" class="use-ajax">' . $year . '</a></li>';
      }
    
      $count = 0;
      foreach ($nids as $item) {
        $node = node_load($item->nid, NULL, TRUE);
        $title = filter_xss($node->title);
        
        if ($is_field) {
          $event_date = field_get_items('node', $node, $timeline_field);
          
          // Support for end date
          if (isset($event_date[0]['value2'])) {
            if ($event_date[0]['value'] == $event_date[0]['value2']) {
              $event_date = date("F j", strtotime($event_date[0]['value']));
            }
            else {
              $event_date = date("F j", strtotime($event_date[0]['value'])) . '-' . date("j", strtotime($event_date[0]['value2']));
            }
          }
          else {
            $event_date = date("F j", strtotime($event_date[0]['value']));
          }
        }
        else {
          $event_date = format_date($node->{$timeline_field}, 'custom', 'F j'); 
        }
        
        if (!empty($timeline_image)) {
          $image = field_get_items('node', $node, $timeline_image);
        }
        $body = field_get_items('node', $node, 'body');
        // Teaser
        if (!empty($body[0]['summary'])) {
          $teaser = strip_tags($body[0]['summary']);
        }
        else {
          $teaser = strip_tags(substr($body[0]['value'], 0, 140));
        }  
        
        $offset = ($count % 2) ? "" : "offset";
        $event_list .= '<li class="event event-current-' . $event_year->year . ' ' . $offset . '">';
        $event_list .= '<h3 class="event-date">' . $event_date . '</h3>';
        $event_list .= '<div class="event-details">' . l($title, 'node/' . $node->nid) . '</div>';
        if (!empty($timeline_image) && !empty($image)) {
          $event_list .= l(theme('image_style', 
            array(
              'style_name' => 'ajax_timeline', 
              'path' => $image[0]['uri'], 
              'alt' => $title, 
              'title' => $title, 
              'attributes' => array('class' => 'event-image')
            )
          ), 'node/' . $node->nid, array('html' => TRUE));
        }
        $event_list .= '<div style="clear:both;"></div>';
        if (!empty($teaser)) {
          $event_list .= '<div class="event-summary">' . $teaser . '... ' . l(t('Read more') . ' &raquo;', 'node/' . $node->nid, array('html' => TRUE)) . '</div>'; 
        }
        $event_list .= '</li>';
        
        $count ++;
      }
    }
    elseif (!$ajax) {
      $event_list .= '<li id="year-' . $event_year->year . '" class="year"><a href="/ajax/ajax_timeline/' . $event_year->year . '" class="use-ajax">' . t("SEE") . ' ' . $event_year->year . '</a></li>';
    }
  }

  if ($ajax) {
    // Append new items
    $commands[] = ajax_command_invoke(NULL, 'ajaxTimelineCallback', array($year, $event_list));
    
    $page = array('#type' => 'ajax', '#commands' => $commands); 
    ajax_deliver($page);
  }
  else{
    $output = '<ul id="ajax-timeline">';
    $output .= $event_list;
    $output .= '</ul>'; 
     
    return $output;  
  }
} 
