
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Bryan Sharpe <bryansharpe@gmail.com>

Ajax Timeline provides a method to view events in a 
formatted timeline based on date

INSTALLATION
------------

Copy the folder into your modules directory


MAINTENANCE
-------------

This project is not actively maintained, but I will try to help 
with any bugs/issues/etc.
